LIBS = -lasan
CFLAGS = -g -Wall

LIBS += $(shell pkg-config --libs talloc libosmocore capi20 spandsp)
CFLAGS += $(shell pkg-config --cflags talloc libosmocore capi20 spandsp)

all: capi-test

clean:
	@rm -f *.o prbs/*.o *.a capi-test

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $^

libprbs.a: prbs/prbs.o prbs/rx.o prbs/tx.o
	$(AR) r $@ $^

capi-test: capi.o capiconn.o bchan.o hdlr_raw_loop.o hdlr_raw_prbs.o libprbs.a hdlr_spandsp.o
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)


