/* Raw/Transparent B-channel with PRBS (pseudo-random bit sequence),
 * useful for BERT (Bit Error Rate Test) */

#include <osmocom/core/utils.h>

#include "bchan.h"
#include "errno.h"
#include "prbs/prbs.h"

#define PRBS_ORDER	11
#define PRBS_BITLEN	(1 << PRBS_ORDER)


struct raw_prbs_priv {
	struct timeslot_state ts;
};

static int raw_prbs_init(struct call_state *cst)
{
	struct raw_prbs_priv *rpp;

	rpp = cst->priv = talloc_zero(cst, struct raw_prbs_priv);
	if (!rpp)
		return -ENOMEM;

	ts_init_prbs_tx(&rpp->ts, 0);
	ts_init_prbs_rx(&rpp->ts, 0);

	return 0;
}

static void raw_prbs_rx(struct call_state *cst, const uint8_t *data, size_t len)
{
	struct raw_prbs_priv *rpp = cst->priv;
	uint8_t tx_buf[len];

	prbs_process_rx(&rpp->ts.rx, data, len);

	/* generate respective number of PRBS bits and transmit them */
	prbs_process_tx(&rpp->ts, tx_buf, len);
	bchan_call_tx(cst, tx_buf, len);
}

static void raw_prbs_fini(struct call_state *cst)
{
	struct raw_prbs_priv *rpp = cst->priv;
	talloc_free(rpp);
	cst->priv = NULL;
}


static struct bchan_handler bch_raw_prbs = {
	.name = "raw_prbs",
	.cfg = {
		.proto = { CAPI_B1_64k_TRANSPARENT, CAPI_B2_TRANSPARENT, CAPI_B3_TRANSPARENT },
		.ncpi = NULL,
		.max_b_data_blocks = 10,
		.max_b_data_len = 32,
	},
	.ops = {
		.init = raw_prbs_init,
		.rx_data = raw_prbs_rx,
		.fini = raw_prbs_fini,
	},
};

static __attribute__((constructor)) void hdlr_raw_prbs_init(void)
{
	bchan_handler_register(&bch_raw_prbs);
}
