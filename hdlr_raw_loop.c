/* Raw/Transparent B-Channel loop-back */

#include "bchan.h"

static int raw_loop_init(struct call_state *cst)
{
	/* no private data to allocate */
	return 0;
}

static void raw_loop_rx(struct call_state *cst, const uint8_t *data, size_t len)
{
	/* echo whatever we received */
	bchan_call_tx(cst, data, len);
}

static void raw_loop_fini(struct call_state *cst)
{
	/* no private data to free */
}


static struct bchan_handler bch_raw_loop = {
	.name = "raw_loop",
	.cfg = {
		.proto = { CAPI_B1_64k_TRANSPARENT, CAPI_B2_TRANSPARENT, CAPI_B3_TRANSPARENT },
		.ncpi = NULL,
		.max_b_data_blocks = 10,
		.max_b_data_len = 32,
	},
	.ops = {
		.init = raw_loop_init,
		.rx_data = raw_loop_rx,
		.fini = raw_loop_fini,
	},
};

static __attribute__((constructor)) void hldr_raw_init(void)
{
	bchan_handler_register(&bch_raw_loop);
}
