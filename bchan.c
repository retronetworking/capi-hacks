/* B-channel handler management.  Handles registration of and routing to
 * various potentially registered B-channel handlers / routes
 *
 * (C) 2022 by Harald Welte <laforge@gnumonks.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <osmocom/core/utils.h>
#include <osmocom/core/linuxlist.h>
#include <string.h>
#include <talloc.h>

#include "bchan.h"

static LLIST_HEAD(g_handlers);
static LLIST_HEAD(g_routes);

struct bchan_handler *bchan_handler_find(const char *name)
{
	struct bchan_handler *bch;

	llist_for_each_entry(bch, &g_handlers, list) {
		if (!strcmp(bch->name, name))
			return bch;
	}
	return NULL;
}

void bchan_handler_register(struct bchan_handler *hdlr)
{
	OSMO_ASSERT(!bchan_handler_find(hdlr->name));
	OSMO_ASSERT(hdlr->list.next == NULL);

	llist_add_tail(&hdlr->list, &g_handlers);
}


void bchan_route_add(int cip, const char *msn, const char *hdlr_name)
{
	struct bchan_handler *bch = bchan_handler_find(hdlr_name);
	struct bchan_route *rt;

	OSMO_ASSERT(bch);

	rt = talloc_zero(g_ctx, struct bchan_route);
	rt->cip = cip;
	rt->msn = talloc_strdup(rt, msn);
	rt->hdlr = bch;

	llist_add_tail(&rt->list, &g_routes);
}


/* find b-channel handler for given CIP + MSN */
struct bchan_handler *bchan_handler_for_call(uint16_t cip, const char *msn)
{
	struct bchan_route *route;

	llist_for_each_entry(route, &g_routes, list) {
		if (route->cip != -1 && route->cip != cip)
			continue;
		if (route->msn && strcmp(route->msn, msn))
			continue;
		return route->hdlr;
	}

	return NULL;
}
