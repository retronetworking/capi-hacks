-- wireshark LUA dissector for the lancapi (ISDN over LAN) protocol used by LANCOM/ELSA/ACOTEC
--
-- (C) 2022 by Martin Hauke <mardnh@gmx.de>
-- SPDX-License-Identifier: GPL-2.0+
--
-- Usage: Move (or symlink) this file to your "personal lua plugins"
-- folder that can be found in the Wireshark Help->About
-- Wireshark->Folders tab Windows: %APPDATA%\Wireshark\plugins.
-- Unix-like systems: ~/.local/lib/wireshark/plugins.


local UDP_PORT_LANCAPI = 75
local lancapi = Proto("lancapi","LANCAPI Protocol")

msg_types = {
	[0x00] = "0x00 - TYPE_UINFO",
	[0x01] = "0x01 - TYPE_CONN_REQ",
	[0x02] = "0x02 - TYPE_CONN_ACK",
	[0x03] = "0x03 - TYPE_DISC_REQ",
	[0x04] = "0x04 - TYPE_DISC_ACK",
	[0x05] = "0x05 - TYPE_DATA_REQ",
	[0x06] = "0x06 - TYPE_DATA_ACK",
	[0x07] = "0x07 - TYPE_KEEP_ACK",
	[0x08] = "0x08 - TYPE_KEEP_REQ",
	[0x09] = "0x09 - Unknown",
}

payload_types = {
	[0x00] = "0x00 - Unknown00",
	[0x01] = "0x01 - RegApplReq",
	[0x02] = "0x02 - RegApplCnf",
	[0x03] = "0x03 - DeRegApplReq",
	[0x04] = "0x04 - DeRegApplCnf",
	[0x05] = "0x05 - BDtaReq",
	[0x06] = "0x06 - BDtaCnf",
	[0x07] = "0x07",                 -- not seen yet
	[0x08] = "0x08 - ConnectReq",
	[0x09] = "0x09 - ConnectCnf",
	[0x10] = "0x10 - DiscCnf",
	[0x11] = "0x11 - DiscInd",
	[0x12] = "0x12 - DiscResp",
	[0x13] = "0x13 - ConnActInd",
	[0x14] = "0x14",                 -- not seen yet
	[0x15] = "0x15 - ProgressInd",
	[0x16] = "0x16 - BConnReq",
	[0x17] = "0x17 - BDtaInd_Set",
	[0x18] = "0x18 - BDtaInd_IsSet",
	[0x19] = "0x19 - BConnConf",
	[0x20] = "0x20 - Unknown20",
	[0x21] = "0x21 - Unknown21",
	[0x22] = "0x22 - BDiscResp",
	[0x0E] = "0x0E - AlertInd",
	[0x1C] = "0x1C - BActiveInd",
	[0x1F] = "0x1F - BDiscInd",
	[0x0A] = "0x0A - ConnectInd",
	[0x0F] = "0x0F - DiscReq",
}

	local f = lancapi.fields;
	f.unknown01 =            ProtoField.uint32 ("lancapi.unknown01", "Unknown01", base.HEX)
	f.unknown02 =            ProtoField.uint8  ("lancapi.unknown02", "Unknown02", base.HEX)
	f.msg_type =             ProtoField.uint8  ("lancapi.msg_type", "MSG_TYPE", nil, msg_types, nil)
	f.seq_no =               ProtoField.uint8  ("lancapi.seq_no", "SeqNo", base.HEX)
	f.len =                  ProtoField.uint8  ("lancapi.len", "Len", base.HEX)
	f.rest_len =             ProtoField.uint8  ("lancapi.rest_len", "RestLen", base.HEX)
	f.conn_id =              ProtoField.uint8  ("lancapi.conn_id", "ConnID", base.HEX)
	f.data_payload =         ProtoField.bytes  ("lancapi.data_payload", "data_payload", base.SPACE)
	f.lancom_capi_msg_type = ProtoField.uint8  ("lancapi.lancom_capi_msg_type", "lancom_capi_msg_type", nil, payload_types, nil)


function lancapi.dissector(buffer,pinfo,tree)
	pinfo.cols.protocol = "LANCAPI"

	local subtree = tree:add(lancapi,buffer(),"LANCAPI Protocol Data")
	local offset = 0

	-- Unknown01 - always "ca e1 ee e3 00" ???
	local unknown01 = buffer(offset,4)
	subtree:add(f.unknown01, unknown01)
	offset = offset + 4

	-- Unknown02 - always "00" ???
	local unknown02 = buffer(offset,1)
	subtree:add(f.unknown02, unknown02)
	offset = offset + 1

	-- MSG_type
	local msg_type = buffer(offset,1):uint()
	subtree:add(f.msg_type, msg_type)
	offset = offset + 1

	-- SeqNo
	local seq_no = buffer(offset,2)
	subtree:add(f.seq_no, seq_no)
	offset = offset + 2

	-- Len
	len = buffer(offset,2)
	subtree:add(f.len, len)
	offset = offset + 2

	-- RestLen
	local rest_len = buffer(offset,2)
	subtree:add(f.rest_len, rest_len)
	offset = offset + 2

	-- ConnID
	local conn_id = buffer(offset,4)
	subtree:add(f.conn_id, conn_id)
	offset = offset + 4

	-- TYPE_DATA payload
	local data_payload = buffer(offset,len:uint())
	subtree:add(f.data_payload, data_payload)

	-- TYPE_DATA lancom_capi_msg_type
	if msg_type == 0x05 then
	  local lancom_capi_msg_type = buffer(offset,1)
	  subtree:add(f.lancom_capi_msg_type, lancom_capi_msg_type)
	end

	---------------------
	-- info column
	---------------------
	if pinfo.dst_port == UDP_PORT_LANCAPI then
	  pinfo.cols.info = string.format("CLIENT -> SERVER : %s", msg_types[msg_type])
	else
	  pinfo.cols.info = string.format("SERVER -> CLIENT : %s", msg_types[msg_type])
	end
end

udp_table = DissectorTable.get("udp.port")
udp_table:add(UDP_PORT_LANCAPI, lancapi)
