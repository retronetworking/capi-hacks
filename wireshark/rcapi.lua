-- wireshark LUA dissector for the rcapi (Remote ISDN CAPI) protocol
-- (C) 2022 by Harald Welte <laforge@gnumonks.org>
-- SPDX-License-Identifier: GPL-2.0+
--
-- Usage: Move (or symlink) this file to your "personal lua plugins"
-- folder that can be found in the Wireshark Help->About
-- Wireshark->Folders tab Windows: %APPDATA%\Wireshark\plugins.
-- Unix-like systems: ~/.local/lib/wireshark/plugins.

rcapi_protocol = Proto("rcapi", "Remote CAPI protocol")

-- table from libcapi20/capicmd.h
local command_ids = {
	[0xf2ff] = "RCAPI_REGISTER_REQ",
	[0xf3ff] = "RCAPI_REGISTER_CONF",
	[0xfaff] = "RCAPI_GET_MANUFACTURER_REQ",
	[0xfbff] = "RCAPI_GET_MANUFACTURER_CONF",
	[0xfcff] = "RCAPI_GET_VERSION_REQ",
	[0xfdff] = "RCAPI_GET_VERSION_CONF",
	[0xfeff] = "RCAPI_GET_SERIAL_NUMBER_REQ",
	[0xffff] = "RCAPI_GET_SERIAL_NUMBER_CONF",
	[0xe0ff] = "RCAPI_GET_PROFILE_REQ",
	[0xe1ff] = "RCAPI_GET_PROFILE_CONF",
	[0xff00] = "RCAPI_AUTH_USER_REQ",
	[0xff01] = "RCAPI_AUTH_USER_CONF",

	[0x8080] = "CAPI_FACILITY_REQ",
	[0x8081] = "CAPI_FACILITY_CONF",
	[0x8082] = "CAPI_FACILITY_IND",
	[0x8083] = "CAPI_FACILITY_RESP",

	[0x0180] = "CAPI_ALERT_REQ",
	[0x0181] = "CAPI_ALERT_CONF",

	[0x0280] = "CAPI_CONNECT_REQ",
	[0x0281] = "CAPI_CONNECT_CONF",
	[0x0282] = "CAPI_CONNECT_IND",
	[0x0283] = "CAPI_CONNECT_RESP",

	[0x8280] = "CAPI_CONNECT_B3_REQ",
	[0x8281] = "CAPI_CONNECT_B3_CONF",
	[0x8282] = "CAPI_CONNECT_B3_IND",
	[0x8283] = "CAPI_CONNECT_B3_RESP",

	[0x0380] = "CAPI_CONNECT_ACTIVE_REQ",
	[0x0381] = "CAPI_CONNECT_ACTIVE_CONF",
	[0x0382] = "CAPI_CONNECT_ACTIVE_IND",
	[0x0383] = "CAPI_CONNECT_ACTIVE_RESP",

	[0x8380] = "CAPI_CONNECT_B3_ACTIVE_REQ",
	[0x8381] = "CAPI_CONNECT_B3_ACTIVE_CONF",
	[0x8382] = "CAPI_CONNECT_B3_ACTIVE_IND",
	[0x8383] = "CAPI_CONNECT_B3_ACTIVE_RESP",

	[0x0480] = "CAPI_DISCONNECT_REQ",
	[0x0481] = "CAPI_DISCONNECT_CONF",
	[0x0482] = "CAPI_DISCONNECT_IND",
	[0x0483] = "CAPI_DISCONNECT_RESP",

	[0x8480] = "CAPI_DISCONNECT_B3_REQ",
	[0x8481] = "CAPI_DISCONNECT_B3_CONF",
	[0x8482] = "CAPI_DISCONNECT_B3_IND",
	[0x8483] = "CAPI_DISCONNECT_B3_RESP",

	[0x0580] = "CAPI_LISTEN_REQ",
	[0x0581] = "CAPI_LISTEN_CONF",

	[0x8680] = "CAPI_DATA_B3_REQ",
	[0x8681] = "CAPI_DATA_B3_CONF",
	[0x8682] = "CAPI_DATA_B3_IND",
	[0x8683] = "CAPI_DATA_B3_RESP",

	[0x8780] = "CAPI_RESET_B3_REQ",
	[0x8781] = "CAPI_RESET_B3_CONF",
	[0x8782] = "CAPI_RESET_B3_IND",
	[0x8783] = "CAPI_RESET_B3_RESP",

	[0x8882] = "CAPI_CONNECT_B3_T90_ACTIVE_IND",
	[0x8883] = "CAPI_CONNECT_B3_T90_ACTIVE_RESP",

	[0x0880] = "CAPI_INFO_REQ",
	[0x0881] = "CAPI_INFO_CONF",
	[0x0882] = "CAPI_INFO_IND",
	[0x0883] = "CAPI_INFO_RESP",

	[0x4180] = "CAPI_SELECT_B_PROTOCOL_REQ",
	[0x4181] = "CAPI_SELECT_B_PROTOCOL_CONF",
}

local reject_vals = {
	[0x0000] = "Accept call",
	[0x0001] = "Ignore call",
	[0x0002] = "Reject call, normal call clearing",
	[0x0003] = "Reject call, user busy",
	[0x0004] = "Reject call, requested circuit/channel not available",
	[0x0005] = "Reject call, facility rejected",
	[0x0006] = "Reject call, incompatible destination",
	[0x0007] = "Reject call, channel unacceptable",
	[0x0008] = "Reject call, destination out of order",
}

-- Table 4-9/Q.931
local ton_vals = {
	[0]	= "Unknown",
	[1]	= "International",
	[2]	= "National",
	[3]	= "Network specific",
	[4]	= "Subscriber",
	[6]	= "Abbreviated",
	[7]	= "Reserved",
}

local npi_vals = {
	[0]	= "Unknown",
	[1]	= "ISDN/Telephony",
	[3]	= "Data",
	[4]	= "Telex",
	[8]	= "National",
	[9]	= "Private",
	[15]	= "Reserved",
}

local f_total_length = ProtoField.uint16("rcapi.total_length", "Total Length", base.DEC)
local f_datapart = ProtoField.bytes("rcapi.datapart", "Data Block")

local f_capi_len = ProtoField.uint16("rcapi.capi_len", "CAPI Message Length", base.DEC)
local f_app_id = ProtoField.uint16("rcapi.app_id", "Application ID", base.HEX)
local f_command_id = ProtoField.uint16("rcapi.command_id", "Command ID", base.HEX_DEC, command_ids)
local f_message_number = ProtoField.uint16("rcapi.message_number", "Message Number", base.DEC)

local f_ncci = ProtoField.uint32("rcapi.ncci", "Network Control Connection ID", base.HEX_DEC)
local f_data_handle = ProtoField.uint16("rcapi.data_handle", "Data Handle", base.HEX)
local f_data_len = ProtoField.uint16("rcapi.data_len", "Data Length", base.DEC)
local f_info = ProtoField.uint16("rcapi.info", "Info", base.HEX)
local f_flags = ProtoField.uint16("rcapi.flags", "Flags", base.HEX)
local f_info_mask = ProtoField.uint32("rcapi.info_mask", "Info Mask", base.HEX)
local f_cip_mask = ProtoField.uint32("rcapi.cip_mask", "CIP Mask", base.HEX)
local f_cip_mask2 = ProtoField.uint32("rcapi.cip_mask2", "CIP Mask 2", base.HEX)
local f_controller = ProtoField.uint32("rcapi.controller", "Controller ID", base.HEX)
local f_plci = ProtoField.uint32("rcapi.plci", "Physical Link Connection ID", base.HEX)
local f_cip_value = ProtoField.uint16("rcapi.cip", "Compatibility Information Profile", base.HEX)
local f_reject = ProtoField.uint16("rcapi.reject", "Reject/Accept call", base.HEX, reject_vals)
local f_info_number = ProtoField.uint16("rcapi.info_number", "Inf number", base.HEX)
local f_info_element = ProtoField.bytes("rcapi.info_element", "Info Element")
local f_called_party = ProtoField.string("rcapi.called_party.number", "Called Party Number")
local f_called_party_ton = ProtoField.uint8("rcapi.called_party.ton", "Called Party Type Of Number", base.HEX, ton_vals, 0x70)
local f_called_party_npi = ProtoField.uint8("rcapi.called_party.npi", "Called Party Numbering Plan ID", base.HEX, npi_vals, 0x0f)
local f_called_party_sub = ProtoField.bytes("rcapi.called_party.subaddr", "Called Party Subaddress")
local f_calling_party = ProtoField.string("rcapi.calling_party.number", "Calling Party Number")
local f_calling_party_ton = ProtoField.uint8("rcapi.calling_party.ton", "Calling Party Type Of Number", base.HEX, ton_vals, 0x70)
local f_calling_party_npi = ProtoField.uint8("rcapi.calling_party.npi", "Calling Party Numbering Plan ID", base.HEX, npi_vals, 0x0f)
local f_calling_party_sub = ProtoField.bytes("rcapi.calling_party.subaddr", "Calling Party Subaddress")
local f_connected = ProtoField.string("rcapi.connected.number", "Connected Number")
local f_connceted_ton = ProtoField.uint8("rcapi.connected.ton", "Connected Number Type Of Number", base.HEX, ton_vals, 0x70)
local f_connected_npi = ProtoField.uint8("rcapi.connected.npi", "Connected NumeberNumbering Plan ID", base.HEX, npi_vals, 0x0f)
local f_connected_sub = ProtoField.bytes("rcapi.connected.subaddr", "Connected Number Subaddress")

local f_bearer_cap = ProtoField.bytes("rcapi.bearer_cap", "Bearer Capabilities")
local f_llc = ProtoField.bytes("rcapi.llc", "Low Layer Compatibility")
local f_hlc = ProtoField.bytes("rcapi.hlc", "High Layer Compatibility")
local f_addl_info = ProtoField.bytes("rcapi.additional_info", "Additional Information")
local f_ncpi = ProtoField.bytes("rcapi.ncpi", "NCPI")
local f_b_protocol = ProtoField.bytes("rcapi.b_protocol", "B Channel Protocol")

local f_Buffer = ProtoField.uint32("rcapi.Buffer", "Buffer pointer", base.HEX)
local f_messageBufferSize = ProtoField.uint16("rcapi.messageBufferSize", "Size of message buffers", base.DEC)
local f_maxLogicalConnections = ProtoField.uint16("rcapi.maxLogicalConnections", "Maximum number of logical connections", base.DEC)
local f_maxBDataBlocks = ProtoField.uint16("rcapi.maxBDataBlocks", "Number of data blocks available simultaneously", base.DEC)
local f_maxBDataLen = ProtoField.uint16("rcapi.maxBDataLen", "Maximum size of a data block", base.DEC)
local f_capiVersion = ProtoField.uint8("rcapi.capiVersion", "CAPI Version to register", base.DEC)

rcapi_protocol.fields = {
	f_total_length, f_datapart,
	f_capi_len, f_app_id, f_command_id, f_message_number,
	f_ncci, f_data_handle, f_data_len, f_info, f_flags, f_info_mask, f_cip_mask, f_cip_mask2,
	f_controller, f_plci, f_cip_value, f_reject, f_info_number, f_info_element,
	f_called_party, f_called_party_ton, f_called_party_npi, f_called_party_sub,
	f_calling_party, f_calling_party_ton, f_calling_party_npi, f_calling_party_sub,
	f_connected, f_connected_ton, f_connected_npi, f_connected_sub,
	f_bearer_cap, f_hlc, f_llc, f_addl_info, f_ncpi, f_b_protocol,
	f_Buffer, f_messageBufferSize, f_maxLogicalConnections, f_maxBDataBlocks, f_maxBDataLen,
	f_capiVersion,
}


-- parse a CAPI 'struct'; return new offset (pointing just after struct) and struct payload
local function parse_struct(tvb, offset)
	local data_len = tvb(offset+0, 1):uint()
	local size
	local data

	if (data_len == 255) then
		data_len = tvb(offset+1, 2):le_uint()
		data = tvb(offset+3, data_len)
		size = 3 + data_len
	elseif (data_len == 0) then
		data = tvb(offset, 0)
		size = 1
	else
		data = tvb(offset+1, data_len)
		size = 1 + data_len
	end
	return offset + size, data
end

local function dissect_called_party(tvb, pinfo, tree)
	if (tvb:len() < 1) then
		tree:add(rcapi_protocol, tvb, "Called Party: <MISSING>")
		return
	end
	local subtree = tree:add(rcapi_protocol, tvb, "Called Party")
	subtree:add(f_called_party_ton, tvb(0, 1))
	subtree:add(f_called_party_npi, tvb(0, 1))
	subtree:add(f_called_party, tvb(1, tvb:len() - 1))
end

local function dissect_calling_party(tvb, pinfo, tree)
	if (tvb:len() < 2) then
		tree:add(rcapi_protocol, tvb, "Calling Party: <MISSING>")
		return
	end
	local subtree = tree:add(rcapi_protocol, tvb, "Calling Party")
	subtree:add(f_calling_party_ton, tvb(0, 1))
	subtree:add(f_calling_party_npi, tvb(0, 1))
	subtree:add(f_calling_party, tvb(2, tvb:len() - 2))
end

local function dissect_connected_number(tvb, pinfo, tree)
	if (tvb:len() < 2) then
		tree:add(rcapi_protocol, tvb, "Connected Number: <MISSING>")
		return
	end
	local subtree = tree:add(rcapi_protocol, tvb, "Connected Number")
	subtree:add(f_connected_ton, tvb(0, 1))
	subtree:add(f_connected_npi, tvb(0, 1))
	subtree:add(f_connected, tvb(2, tvb:len() - 2))
end



-- RCAPI Section 2.1
local function dissect_register_req(tvb, pinfo, tree)
	tree:add_le(f_Buffer, tvb(0, 4))
	tree:add_le(f_messageBufferSize, tvb(4, 2))
	tree:add_le(f_maxLogicalConnections, tvb(6, 2))
	tree:add_le(f_maxBDataBlocks, tvb(8, 2))
	tree:add_le(f_maxBDataLen, tvb(10, 2))
	tree:add(f_capiVersion, tvb(12, 1))
end

-- RCAPI Section 2.2
local function dissect_register_conf(tvb, pinfo, tree)
	tree:add_le(f_info, tvb(0, 2))
end

-- Section 5.5
local function dissect_connect_ind(tvb, pinfo, tree)
	tree:add_le(f_plci, tvb(0, 4))
	tree:add_le(f_cip_value, tvb(4, 2))
	local offset = 6
	offset, called_party = parse_struct(tvb, offset)
	dissect_called_party(called_party, pinfo, tree)
	offset, calling_party = parse_struct(tvb, offset)
	dissect_calling_party(calling_party, pinfo, tree)
	offset, called_party_sub = parse_struct(tvb, offset)
	tree:add(f_called_party_sub, called_party_sub)
	offset, calling_party_sub = parse_struct(tvb, offset)
	tree:add(f_calling_party_sub, called_party_sub)
	offset, bearer_cap = parse_struct(tvb, offset)
	tree:add(f_bearer_cap, bearer_cap)
	offset, llc = parse_struct(tvb, offset)
	tree:add(f_llc, llc)
	offset, hlc = parse_struct(tvb, offset)
	tree:add(f_hlc, hlc)
	offset, addl_info = parse_struct(tvb, offset)
	tree:add(f_addl_info, addl_info)
	if (tvb:len() > offset) then
		offset, second_calling_party = parse_struct(tvb, offset)
		dissect_calling_party(second_calling_party, pinfo, tree)
	end
end

-- Section 5.6
local function dissect_connect_resp(tvb, pinfo, tree)
	tree:add_le(f_plci, tvb(0, 4))
	tree:add_le(f_reject, tvb(4, 2))
	local offset = 6
	offset, b_protocol = parse_struct(tvb, offset)
	tree:add(f_b_protocol, b_protocol)
	offset, connected_number = parse_struct(tvb, offset)
	dissect_connected_number(connected_number, pinfo, tree)
	offset, connected_sub = parse_struct(tvb, offset)
	tree:add(f_connected_sub, connected_sub)
	offset, llc = parse_struct(tvb, offset)
	tree:add(f_llc, llc)
	offset, addl_info = parse_struct(tvb, offset)
	tree:add(f_addl_info, addl_info)
end

-- Section 5.7
local function dissect_connect_active_ind(tvb, pinfo, tree)
	tree:add_le(f_plci, tvb(0, 4))
	local offset = 4
	offset, connected_number = parse_struct(tvb, offset)
	dissect_connected_number(connected_number, pinfo, tree)

	offset, connected_sub = parse_struct(tvb, offset)
	tree:add(f_connected_sub, connected_sub)

	offset, llc = parse_struct(tvb, offset)
	tree:add(f_llc, llc)
end

-- Section 5.8
local function dissect_connect_active_resp(tvb, pinfo, tree)
	tree:add_le(f_plci, tvb(0, 4))
end

-- Section 5.9
local function dissect_connect_b3_active_ind(tvb, pinfo, tree)
	tree:add_le(f_ncci, tvb(0, 4))
	local offset = 4
	offset, ncpi = parse_struct(tvb, offset)
	tree:add(f_ncpi, ncpi)
end

-- Section 5.10
local function dissect_connect_b3_active_resp(tvb, pinfo, tree)
	tree:add_le(f_ncci, tvb(0, 4))
end

-- Section 5.13
local function dissect_connect_b3_ind(tvb, pinfo, tree)
	tree:add_le(f_ncci, tvb(0, 4))
	local offset = 4
	offset, ncpi = parse_struct(tvb, offset)
	tree:add(f_ncpi, ncpi)
end

-- Section 5.14
local function dissect_connect_b3_resp(tvb, pinfo, tree)
	tree:add_le(f_ncci, tvb(0, 4))
end

-- Section 5.17
local function dissect_data_b3_req(tvb, pinfo, tree)
	tree:add_le(f_ncci, tvb(0, 4))
	-- data tvb(4, 4)
	tree:add_le(f_data_len, tvb(8, 2))
	tree:add_le(f_data_handle, tvb(10, 2))
	tree:add_le(f_flags, tvb(12, 2))
	-- data64
end

-- Section 5.18
local function dissect_data_b3_conf(tvb, pinfo, tree)
	tree:add_le(f_ncci, tvb(0, 4))
	tree:add_le(f_data_handle, tvb(4, 2))
	tree:add_le(f_info, tvb(6, 2))
end

-- Section 5.19
local function dissect_data_b3_ind(tvb, pinfo, tree)
	tree:add_le(f_ncci, tvb(0, 4))
	-- data tvb(4, 4)
	tree:add_le(f_data_len, tvb(8, 2))
	tree:add_le(f_data_handle, tvb(10, 2))
	tree:add_le(f_flags, tvb(12, 2))
	-- data64
end

-- Section 5.20
local function dissect_data_b3_resp(tvb, pinfo, tree)
	tree:add_le(f_ncci, tvb(0, 4))
	tree:add_le(f_data_handle, tvb(4, 2))
end

-- Section 5.35
local function dissect_info_ind(tvb, pinfo, tree)
	tree:add_le(f_plci, tvb(0, 4))
	tree:add_le(f_info_number, tvb(4, 2))
	local offset = 6
	offset, info_element = parse_struct(tvb, offset)
	tree:add(f_info_element, info_element)
end

-- Section 5.36
local function dissect_info_resp(tvb, pinfo, tree)
	tree:add_le(f_plci, tvb(0, 4))
end

-- Section 5.37
local function dissect_listen_req(tvb, pinfo, tree)
	tree:add_le(f_controller, tvb(0, 4))
	tree:add_le(f_info_mask, tvb(4, 4))
	tree:add_le(f_cip_mask, tvb(8, 4))
	tree:add_le(f_cip_mask2, tvb(12, 4))
	local offset = 16
	offset, calling_party = parse_struct(tvb, offset)
	dissect_calling_party(calling_party, pinfo, tree)
	offset, calling_party_sub = parse_struct(tvb, offset)
	tree:add(f_calling_party_sub, called_party_sub)
end

-- Section 5.38
local function dissect_listen_conf(tvb, pinfo, tree)
	tree:add_le(f_controller, tvb(0, 4))
	tree:add_le(f_info, tvb(4, 2))
end


-- CAPI common header, See Section 3.3 Message Structure
local function capi_dissect_pdu(tvb, pinfo, tree)
	tree:add_le(f_capi_len, tvb(0, 2))
	tree:add_le(f_app_id, tvb(2, 2))
	tree:add(f_command_id, tvb(4, 2))
	tree:add_le(f_message_number, tvb(6, 2))

	local capi_len = tvb(0, 2):le_uint()
	local command_id = tvb(4, 2):uint()

	capi_data = tvb(8, capi_len-8)
	if (command_id == 0xf2ff) then
		dissect_register_req(capi_data(), pinfo, tree)
	elseif (command_id == 0xf3ff) then
		dissect_register_conf(capi_data(), pinfo, tree)
	elseif (command_id == 0x8680) then
		dissect_data_b3_req(capi_data(), pinfo, tree)
	elseif (command_id == 0x8681) then
		dissect_data_b3_conf(capi_data(), pinfo, tree)
	elseif (command_id == 0x8682) then
		dissect_data_b3_ind(capi_data(), pinfo, tree)
	elseif (command_id == 0x8683) then
		dissect_data_b3_resp(capi_data(), pinfo, tree)
	elseif (command_id == 0x0580) then
		dissect_listen_req(capi_data(), pinfo, tree)
	elseif (command_id == 0x0581) then
		dissect_listen_conf(capi_data(), pinfo, tree)
	elseif (command_id == 0x0282) then
		dissect_connect_ind(capi_data(), pinfo, tree)
	elseif (command_id == 0x0283) then
		dissect_connect_resp(capi_data(), pinfo, tree)
	elseif (command_id == 0x0382) then
		dissect_connect_active_ind(capi_data(), pinfo, tree)
	elseif (command_id == 0x0383) then
		dissect_connect_active_resp(capi_data(), pinfo, tree)
	elseif (command_id == 0x8382) then
		dissect_connect_b3_active_ind(capi_data(), pinfo, tree)
	elseif (command_id == 0x8383) then
		dissect_connect_b3_active_resp(capi_data(), pinfo, tree)
	elseif (command_id == 0x8282) then
		dissect_connect_b3_ind(capi_data(), pinfo, tree)
	elseif (command_id == 0x8283) then
		dissect_connect_b3_resp(capi_data(), pinfo, tree)
	elseif (command_id == 0x0882) then
		dissect_info_ind(capi_data(), pinfo, tree)
	elseif (command_id == 0x0883) then
		dissect_info_resp(capi_data(), pinfo, tree)
	end

	pinfo.cols.info = string.format("%s", command_ids[command_id])
end


local function rcapi_dissect_pdu(buffer, pinfo, tree)
	length = buffer:len()
	if length == 0 then return end

	pinfo.cols.protocol = rcapi_protocol.name

	local subtree = tree:add(rcapi_protocol, buffer(), "Remote CAPI")
	local total_len = buffer(0, 2):uint()
	local capi_len =  buffer(2, 2):le_uint()

	local capi_tvb = buffer(2, capi_len)

	subtree:add(f_total_length, buffer(0, 2))

	capi_dissect_pdu(capi_tvb, pinfo, subtree)

	if (total_len - (2+capi_len) > 0) then
		subtree:add(f_datapart, buffer(2+capi_len, total_len - (2+capi_len)))
	end
end

-- A Lua function that will be called for each PDU, to determine the
-- full length of the PDU. The called function will be given (1) the Tvb
-- object of the whole Tvb (possibly reassembled), (2) the Pinfo object,
-- and (3) an offset number of the index of the first byte of the PDU
-- (i.e., its first header byte). The Lua function must return a Lua
-- number of the full length of the PDU.
local function rcapi_get_len_func(tvb, pinfo, offset)
	return tvb:range(offset, 2):uint()
end

function rcapi_protocol.dissector(tvb, pinfo, tree)
	dissect_tcp_pdus(tvb, tree, 12, rcapi_get_len_func, rcapi_dissect_pdu)
	return tvb:len()
end

function rcapi_protocol.init()
	DissectorTable.get("tcp.port"):add(2662, rcapi_protocol)
end
