#pragma once

/* B-channel handler management.  Handles registration of and routing to
 * various potentially registered B-channel handlers / routes
 *
 * (C) 2022 by Harald Welte <laforge@gnumonks.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <stdint.h>
#include <osmocom/core/linuxlist.h>

#include "capi_misc_defs.h"

enum log_subsys {
	DLCAPI20,
	DCAPI,
};

extern void *g_ctx;

struct capi_connection;

struct bchan_handler_cfg {
	/* B1/B2/B3 protocol used in CAPI CONNECT_RESP */
	struct {
		uint16_t b1;
		uint16_t b2;
		uint16_t b3;
	} proto;
	const uint8_t *ncpi;
	uint32_t max_b_data_blocks;
	uint32_t max_b_data_len;
};

struct call_state;

/* call-back functions (operations) for a B-channel handler */
struct bchan_handler_ops {
	/* initialize whatever handler-private state (call setup) */
	int (*init)(struct call_state *cst);
	/* receive data (ISDN -> handler) */
	void (*rx_data)(struct call_state *cst, const uint8_t *data, size_t len);
	/* destroy whatever handler-private state (call teardown) */
	void (*fini)(struct call_state *cst);
};

/* The actual B-channel handler: list/name/config/ops */
struct bchan_handler {
	struct llist_head list;
	/* name of this handler / application */
	const char *name;
	/* CAPI related configuration */
	struct bchan_handler_cfg cfg;
	struct bchan_handler_ops ops;

	/* handler private data */
	void *priv;
};

/* representation of one given call within a specific bchan-handler */
struct call_state {
	/* back-pointer to handler */
	struct bchan_handler *bch;
	/* capiconn.h state */
	struct capi_connection *cc;
	/* per-call private state (return value of bchan_handler.init) */
	void *priv;
};


/* a route for routing incoming calls to a bchan-handler */
struct bchan_route {
	/* global list of bchan_routes */
	struct llist_head list;
	int cip;			/* -1: accept any CIP */
	const char *msn;		/* NULL: accept any MSN / called number */
	struct bchan_handler *hdlr;	/* handler for this CIP/MSN */
};

struct bchan_handler *bchan_handler_find(const char *name);
void bchan_handler_register(struct bchan_handler *hdlr);
void bchan_route_add(int cip, const char *msn, const char *hdlr_name);
struct bchan_handler *bchan_handler_for_call(uint16_t cip, const char *msn);

int bchan_call_tx(struct call_state *cst, const uint8_t *data, size_t len);
